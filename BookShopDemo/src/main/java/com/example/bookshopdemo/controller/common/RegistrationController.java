package com.example.bookshopdemo.controller.common;

import com.example.bookshopdemo.error.EmailAlreadyExistsException;
import com.example.bookshopdemo.model.dto.UserDto;
import com.example.bookshopdemo.service.interfaces.common.UserServiceCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class RegistrationController {

    private final UserServiceCommon userServiceCommon;

    @Autowired
    public RegistrationController(@Qualifier("userServiceCommonImpl") UserServiceCommon userServiceCommon) {
        this.userServiceCommon = userServiceCommon;
    }

    @GetMapping("/registration")
    public String getRegistrationPage(Model model) {

        model.addAttribute("message", "Please, enter your data: ");
        return "registration";
    }

    @PostMapping("/registration")
    public String registrationPage(UserDto userDto) {

        userServiceCommon.saveUser(userDto);
        return "redirect:/login";
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @GetMapping("/error/emailAlreadyExists")
    public String onNoSuchBooError(EmailAlreadyExistsException e, Model model) {

        model.addAttribute("message", e.getMessage());
        return "registration";
    }
}
