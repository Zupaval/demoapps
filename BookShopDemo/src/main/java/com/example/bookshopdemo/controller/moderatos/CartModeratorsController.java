package com.example.bookshopdemo.controller.moderatos;

import com.example.bookshopdemo.error.BookNotExistsInCartException;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.error.NoSuchCartException;
import com.example.bookshopdemo.service.interfaces.common.CartServiceCommon;
import com.example.bookshopdemo.service.interfaces.common.messages.CartServiceCommonMessages;
import com.example.bookshopdemo.service.interfaces.moderators.CartServiceModerators;
import com.example.bookshopdemo.service.interfaces.moderators.messages.CartServicesModeratorsMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/cartModerators")
public class CartModeratorsController {

    private final CartServiceCommon cartServiceCommon;
    private final CartServiceCommonMessages cartServiceCommonMessages;
    private final CartServiceModerators cartServiceModerators;
    private final CartServicesModeratorsMessages cartServicesModeratorsMessages;

    @Autowired
    public CartModeratorsController(@Qualifier("cartServiceModeratorsImpl") CartServiceCommon cartServiceCommon,
                                    @Qualifier("cartServiceModeratorsMessagesImpl") CartServiceCommonMessages
                                            cartServiceCommonMessages, CartServiceModerators cartServiceModerators,
                                    CartServicesModeratorsMessages cartServicesModeratorsMessages) {
        this.cartServiceCommon = cartServiceCommon;
        this.cartServiceCommonMessages = cartServiceCommonMessages;
        this.cartServiceModerators = cartServiceModerators;
        this.cartServicesModeratorsMessages = cartServicesModeratorsMessages;
    }

    @GetMapping("/")
    public String myCart(Model model) {

        model.addAttribute("carts", List.of(cartServiceCommon.findCart()));
        model.addAttribute("quantity", cartServiceCommon.totalQuantityOfCart());
        model.addAttribute("coast", cartServiceCommon.totalCoastOfCart());
        model.addAttribute("message", cartServiceCommonMessages.messageOfMyCart());
        return "cartModerators";
    }

    @PostMapping("/findCartById")
    public String findCartById(Long cartId, Model model) {

        model.addAttribute("carts", List.of(cartServiceModerators.findCartById(cartId)));
        model.addAttribute("quantity", cartServiceModerators.totalQuantityOfCartById(cartId));
        model.addAttribute("coast", cartServiceModerators.totalCoastOfCartById(cartId));
        model.addAttribute("message", cartServicesModeratorsMessages.messageOfCartById(cartId));
        return "cartModerators";
    }

    @PostMapping("/findAllCarts")
    public String findAllCarts(Model model) {

        model.addAttribute("carts", cartServiceModerators.findAllCarts());
        model.addAttribute("quantity", cartServiceModerators.totalQuantityOfBooksOfAllCarts());
        model.addAttribute("coast", cartServiceModerators.totalCoastOfBooksOfAllCarts());
        model.addAttribute("message", cartServicesModeratorsMessages.messageOfAllCarts());
        return "cartModerators";
    }

    @PostMapping("/addBook")
    public String addBook(Long cartId, Long id, Model model) {

        model.addAttribute("carts", List.of(cartServiceModerators.addBookToCartById(cartId, id)));
        model.addAttribute("quantity", cartServiceModerators.totalQuantityOfCartById(cartId));
        model.addAttribute("coast", cartServiceModerators.totalCoastOfCartById(cartId));
        model.addAttribute("message", cartServicesModeratorsMessages.messageOfAddBookToCartById(cartId, id));
        return "cartModerators";
    }

    @PostMapping("/deleteBook")
    public String deleteBook(Long cartId, Long id, Model model) {

        model.addAttribute("carts", List.of(cartServiceModerators.deleteBookFromCartById(cartId, id)));
        model.addAttribute("quantity", cartServiceModerators.totalQuantityOfCartById(cartId));
        model.addAttribute("coast", cartServiceModerators.totalCoastOfCartById(cartId));
        model.addAttribute("message", cartServicesModeratorsMessages.messageOfDeleteBookFromCartById(cartId, id));
        return "cartModerators";
    }

    @PostMapping("/deleteAllBooks")
    public String deleteAllBooks(Long cartId, Model model) {

        model.addAttribute("carts", List.of(cartServiceModerators.deleteAllBooksFromCartById(cartId)));
        model.addAttribute("quantity", cartServiceModerators.totalQuantityOfCartById(cartId));
        model.addAttribute("coast", cartServiceModerators.totalCoastOfCartById(cartId));
        model.addAttribute("message", cartServicesModeratorsMessages.messageOfDeleteAllBooksFromCartById(cartId));
        return "cartModerators";
    }

    @ExceptionHandler(NoSuchBookException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/noSuchBook")
    public String onNoSuchBookError(NoSuchBookException e, Model model) {

        model.addAttribute("carts", cartServiceModerators.findAllCarts());
        model.addAttribute("quantity", cartServiceModerators.totalQuantityOfBooksOfAllCarts());
        model.addAttribute("coast", cartServiceModerators.totalCoastOfBooksOfAllCarts());
        model.addAttribute("message", e.getMessage());
        return "cartModerators";
    }

    @ExceptionHandler(NoSuchCartException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/noSuchCart")
    public String onNoSuchCartError(NoSuchCartException e, Model model) {

        model.addAttribute("carts", cartServiceModerators.findAllCarts());
        model.addAttribute("quantity", cartServiceModerators.totalQuantityOfBooksOfAllCarts());
        model.addAttribute("coast", cartServiceModerators.totalCoastOfBooksOfAllCarts());
        model.addAttribute("message", e.getMessage());
        return "cartModerators";
    }

    @ExceptionHandler(BookNotExistsInCartException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/bookNotExistsInCart")
    public String onBookNotExistsInCartError(BookNotExistsInCartException e, Model model) {

        model.addAttribute("carts", cartServiceModerators.findAllCarts());
        model.addAttribute("quantity", cartServiceModerators.totalQuantityOfBooksOfAllCarts());
        model.addAttribute("coast", cartServiceModerators.totalCoastOfBooksOfAllCarts());
        model.addAttribute("message", e.getMessage());
        return "cartModerators";
    }
}
