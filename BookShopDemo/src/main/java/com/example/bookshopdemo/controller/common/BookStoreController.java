package com.example.bookshopdemo.controller.common;

import com.example.bookshopdemo.model.dto.UserDto;
import com.example.bookshopdemo.service.interfaces.common.UserServiceCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BookStoreController {

    private final UserServiceCommon userServiceCommon;

    @Autowired
    public BookStoreController(@Qualifier("userServiceCommonImpl") UserServiceCommon userServiceCommon) {
        this.userServiceCommon = userServiceCommon;
    }

    @GetMapping("/bookStore")
    public String bookStore(Model model) {

        UserDto currentUser = userServiceCommon.findCurrentUser();
        model.addAttribute("firstname", currentUser.getFirstName());
        model.addAttribute("lastname", currentUser.getLastName());
        return "bookStore";
    }
}