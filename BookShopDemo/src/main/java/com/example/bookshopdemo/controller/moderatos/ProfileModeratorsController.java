package com.example.bookshopdemo.controller.moderatos;

import com.example.bookshopdemo.error.NoSuchRoleException;
import com.example.bookshopdemo.error.NoSuchUserException;
import com.example.bookshopdemo.service.interfaces.common.UserServiceCommon;
import com.example.bookshopdemo.service.interfaces.common.messages.UserServiceCommonMessages;
import com.example.bookshopdemo.service.interfaces.moderators.UserServiceModerators;
import com.example.bookshopdemo.service.interfaces.moderators.messages.UserServiceModeratorsMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/profileModerators")
public class ProfileModeratorsController {

    private final UserServiceCommon userServiceCommon;
    private final UserServiceCommonMessages userServiceCommonMessages;
    private final UserServiceModerators userServiceModerators;
    private final UserServiceModeratorsMessages userServiceModeratorsMessages;

    @Autowired
    public ProfileModeratorsController(@Qualifier("userServiceModeratorsImpl") UserServiceCommon userServiceCommon,
                                       @Qualifier("userServiceModeratorsMessagesImpl") UserServiceCommonMessages
                                               userServiceCommonMessages, UserServiceModerators userServiceModerators,
                                       UserServiceModeratorsMessages userServiceModeratorsMessages) {
        this.userServiceCommon = userServiceCommon;
        this.userServiceCommonMessages = userServiceCommonMessages;
        this.userServiceModerators = userServiceModerators;
        this.userServiceModeratorsMessages = userServiceModeratorsMessages;
    }

    @GetMapping("/")
    public String myProfile(Model model) {

        model.addAttribute("users", List.of(userServiceCommon.findCurrentUser()));
        model.addAttribute("message", userServiceCommonMessages.getMessageOfCurrentUser());
        return "profileModerators";
    }

    @PostMapping("/findById")
    public String userById(Long id, Model model) {

        model.addAttribute("users", List.of(userServiceModerators.findUserById(id)));
        model.addAttribute("message", userServiceModeratorsMessages.getMessageOfUserById(id));
        return "profileModerators";
    }

    @PostMapping("/findByEmail")
    public String userByEmail(String email, Model model) {

        model.addAttribute("users", List.of(userServiceModerators.findUserByEmail(email)));
        model.addAttribute("message", userServiceModeratorsMessages.getMessageOfUserByEmail(email));
        return "profileModerators";
    }

    @PostMapping("/findByLastName")
    public String userByLastName(String lastName, Model model) {

        model.addAttribute("users", userServiceModerators.findUserByLastName(lastName));
        model.addAttribute("message", userServiceModeratorsMessages.getMessageOfUserByLastName(lastName));
        return "profileModerators";
    }

    @PostMapping("/all")
    public String findAll(Model model) {

        model.addAttribute("users", userServiceModerators.findAllUsers());
        model.addAttribute("message", userServiceModeratorsMessages.getMessageOfAllUsers());
        return "profileModerators";
    }

    @PostMapping("/addRole")
    public String addUserRole(Long UserId, Long RoleId, Model model) {

        model.addAttribute("users", List.of(userServiceModerators.addUserRole(UserId, RoleId)));
        model.addAttribute("message", userServiceModeratorsMessages.getMessageOfAddUserRole(UserId, RoleId));
        return "profileModerators";
    }

    @PostMapping("/deleteRole")
    public String deleteUserRole(Long UserId, Long RoleId, Model model) {

        model.addAttribute("users", List.of(userServiceModerators.deleteUserRole(UserId, RoleId)));
        model.addAttribute("message", userServiceModeratorsMessages.getMessageOfDeleteUserRole(UserId, RoleId));
        return "profileModerators";
    }

    @PostMapping("/delete")
    public String deleteUser(Long id, Model model) {

        model.addAttribute("users", List.of(userServiceModerators.deleteUserById(id)));
        model.addAttribute("message", userServiceModeratorsMessages.getMessageOfDeleteUserById(id));
        return "profileModerators";
    }

    @ExceptionHandler(NoSuchUserException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/noSuchUser")
    public String onNoSuchUserError(NoSuchUserException e, Model model) {

        model.addAttribute("users", userServiceModerators.findUserByLastName(""));
        model.addAttribute("message", e.getMessage());
        return "profileModerators";
    }

    @ExceptionHandler(NoSuchRoleException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/noSuchRole")
    public String onNoSuchRoleError(NoSuchRoleException e, Model model) {

        model.addAttribute("users", userServiceModerators.findUserByLastName(""));
        model.addAttribute("message", e.getMessage());
        return "profileModerators";
    }
}

