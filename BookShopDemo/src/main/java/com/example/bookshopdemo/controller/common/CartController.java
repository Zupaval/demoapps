package com.example.bookshopdemo.controller.common;

import com.example.bookshopdemo.service.interfaces.common.CartServiceCommon;
import com.example.bookshopdemo.service.interfaces.common.messages.CartServiceCommonMessages;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cart")
public class CartController {

    private final CartServiceCommon cartServiceCommon;
    private final CartServiceCommonMessages cartServiceCommonMessages;

    public CartController(@Qualifier("cartServiceCommonImpl") CartServiceCommon cartServiceCommon,
                          @Qualifier("cartServiceCommonMessagesImpl") CartServiceCommonMessages
                                  cartServiceCommonMessages) {
        this.cartServiceCommon = cartServiceCommon;
        this.cartServiceCommonMessages = cartServiceCommonMessages;
    }

    @GetMapping("/")
    public String myCart(Model model) {

        model.addAttribute("cartId", cartServiceCommon.findCart().getId());
        model.addAttribute("books", cartServiceCommon.findCart().getBooks());
        model.addAttribute("quantity", cartServiceCommon.totalQuantityOfCart());
        model.addAttribute("coast", cartServiceCommon.totalCoastOfCart());
        model.addAttribute("message", cartServiceCommonMessages.messageOfMyCart());
        return "cart";
    }

    @PostMapping("/addBook")
    public String addBook(Long id, Model model) {

        model.addAttribute("cartId", cartServiceCommon.findCart().getId());
        model.addAttribute("books", cartServiceCommon.addBookToCart(id).getBooks());
        model.addAttribute("quantity", cartServiceCommon.totalQuantityOfCart());
        model.addAttribute("coast", cartServiceCommon.totalCoastOfCart());
        model.addAttribute("message", cartServiceCommonMessages.messageOfAddBookToMyCart(id));
        return "cart";
    }

    @PostMapping("/deleteBook")
    public String deleteBook(Long id, Model model) {

        model.addAttribute("cartId", cartServiceCommon.findCart().getId());
        model.addAttribute("books", cartServiceCommon.deleteBookFromCart(id).getBooks());
        model.addAttribute("quantity", cartServiceCommon.totalQuantityOfCart());
        model.addAttribute("coast", cartServiceCommon.totalCoastOfCart());
        model.addAttribute("message", cartServiceCommonMessages.messageOfDeleteBookFromMyCart(id));
        return "cart";
    }

    @PostMapping("/deleteAllBooks")
    public String deleteAllBooks(Model model) {

        model.addAttribute("cartId", cartServiceCommon.findCart().getId());
        model.addAttribute("books", cartServiceCommon.deleteAllBooksFromCart().getBooks());
        model.addAttribute("quantity", cartServiceCommon.totalQuantityOfCart());
        model.addAttribute("coast", cartServiceCommon.totalCoastOfCart());
        model.addAttribute("message", cartServiceCommonMessages.messageOfDeleteAllBooksFromMyCart());
        return "cart";
    }
}
