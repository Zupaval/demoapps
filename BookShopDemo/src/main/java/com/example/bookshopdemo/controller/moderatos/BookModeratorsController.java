package com.example.bookshopdemo.controller.moderatos;

import com.example.bookshopdemo.error.BookAlreadyExistsException;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.model.dto.BookDto;
import com.example.bookshopdemo.service.interfaces.common.BookServiceCommon;
import com.example.bookshopdemo.service.interfaces.common.messages.BookServiceCommonMessages;
import com.example.bookshopdemo.service.interfaces.moderators.BookServiceModerators;
import com.example.bookshopdemo.service.interfaces.moderators.messages.BookServiceModeratorsMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/bookModerators")
public class BookModeratorsController {

    private final BookServiceCommon bookServiceCommon;
    private final BookServiceCommonMessages bookServiceCommonMessages;
    private final BookServiceModerators bookServiceModerators;
    private final BookServiceModeratorsMessages bookServiceModeratorsMessages;

    @Autowired
    public BookModeratorsController(@Qualifier("bookServiceModeratorsImpl") BookServiceCommon bookServiceCommon,
                                    @Qualifier("bookServiceModeratorsMessagesImpl") BookServiceCommonMessages
                                            bookServiceCommonMessages, BookServiceModerators bookServiceModerators,
                                    BookServiceModeratorsMessages bookServiceModeratorsMessages) {
        this.bookServiceCommon = bookServiceCommon;
        this.bookServiceCommonMessages = bookServiceCommonMessages;
        this.bookServiceModerators = bookServiceModerators;
        this.bookServiceModeratorsMessages = bookServiceModeratorsMessages;
    }

    @GetMapping("/")
    public String managementBooks(Model model) {

        model.addAttribute("books", bookServiceCommon.findAllBooks());
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfAllBooks());
        return "bookModerators";
    }

    @PostMapping("/findBookById")
    public String findBookById(Long id, Model model) {

        model.addAttribute("books", List.of(bookServiceCommon.findBookById(id)));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBookById(id));
        return "bookModerators";
    }

    @PostMapping("/findBookByAuthor")
    public String findBookByAuthor(String author, Model model) {

        model.addAttribute("books", bookServiceCommon.findBookByAuthor(author));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBookByAuthor(author));
        return "bookModerators";
    }

    @PostMapping("/findBookByBookName")
    public String findBookByBookName(String bookName, Model model) {

        model.addAttribute("books", bookServiceCommon.findBookByBookName(bookName));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBookByBookName(bookName));
        return "bookModerators";
    }

    @PostMapping("/findAllBooks")
    public String findAllBooks(Model model) {

        model.addAttribute("books", bookServiceCommon.findAllBooks());
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfAllBooks());
        return "bookModerators";
    }

    @PostMapping("/createBook")
    public String createBook(BookDto bookDto, Model model) {

        model.addAttribute("books", List.of(bookServiceModerators.saveBook(bookDto)));
        model.addAttribute("message", bookServiceModeratorsMessages.getMessageOfSaveBook(bookDto));
        return "bookModerators";
    }

    @PostMapping("/updateBook")
    public String updateBook(BookDto bookDto, Model model) {

        model.addAttribute("books", List.of(bookServiceModerators.updateBook(bookDto)));
        model.addAttribute("message", bookServiceModeratorsMessages.getMessageOfUpdateBook(bookDto));
        return "bookModerators";
    }

    @PostMapping("/deleteBook")
    public String deleteBook(Long id, Model model) {

        model.addAttribute("books", List.of(bookServiceModerators.deleteBookById(id)));
        model.addAttribute("message", bookServiceModeratorsMessages.getMessageOfDeleteBook(id));
        return "bookModerators";
    }

    @ExceptionHandler(NoSuchBookException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/noSuchBook")
    public String onNoSuchBookError(NoSuchBookException e, Model model) {

        model.addAttribute("books", bookServiceCommon.findBookByAuthor(""));
        model.addAttribute("message", e.getMessage());
        return "bookModerators";
    }

    @ExceptionHandler(BookAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/bookAlreadyExists")
    public String onBookAlreadyExistsError(BookAlreadyExistsException e, Model model) {

        model.addAttribute("books", bookServiceCommon.findAllBooks());
        model.addAttribute("message", e.getMessage());
        return "bookModerators";
    }
}