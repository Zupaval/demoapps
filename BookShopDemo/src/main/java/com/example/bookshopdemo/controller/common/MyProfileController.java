package com.example.bookshopdemo.controller.common;

import com.example.bookshopdemo.model.dto.UserDto;
import com.example.bookshopdemo.service.interfaces.common.UserServiceCommon;
import com.example.bookshopdemo.service.interfaces.common.messages.UserServiceCommonMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/profile")
public class MyProfileController {

    private final UserServiceCommon userServiceCommon;
    private final UserServiceCommonMessages userServiceCommonMessages;

    @Autowired
    public MyProfileController(@Qualifier("userServiceCommonImpl") UserServiceCommon userServiceCommon,
                               @Qualifier("userServiceCommonMessagesImpl") UserServiceCommonMessages
                                       userServiceCommonMessages) {
        this.userServiceCommon = userServiceCommon;
        this.userServiceCommonMessages = userServiceCommonMessages;
    }

    @GetMapping("/")
    public String myProfile(Model model) {

        model.addAttribute("users", List.of(userServiceCommon.findCurrentUser()));
        model.addAttribute("message", userServiceCommonMessages.getMessageOfCurrentUser());
        return "profile";
    }

    @PostMapping("/update")
    public String updateMyProfile(UserDto userDto, Model model) {

        model.addAttribute("users", List.of(userServiceCommon.updateUser(userDto)));
        model.addAttribute("message", userServiceCommonMessages.getMessageOfUpdateUser());
        return "profile";
    }
}
