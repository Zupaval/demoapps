package com.example.bookshopdemo.controller.moderatos;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ModeratorsController {

    @GetMapping("/moderators")
    public String adminModerators() {

        return "moderators";
    }
}
