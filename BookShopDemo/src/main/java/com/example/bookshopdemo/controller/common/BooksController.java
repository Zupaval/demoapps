package com.example.bookshopdemo.controller.common;

import com.example.bookshopdemo.error.BookAlreadyExistsException;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.service.interfaces.common.BookServiceCommon;
import com.example.bookshopdemo.service.interfaces.common.messages.BookServiceCommonMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/books")
public class BooksController {

    private final BookServiceCommon bookServiceCommon;
    private final BookServiceCommonMessages bookServiceCommonMessages;

    @Autowired
    public BooksController(@Qualifier("bookServiceCommonImpl") BookServiceCommon bookServiceCommon,
                           @Qualifier("bookServiceCommonMessagesImpl") BookServiceCommonMessages
                                   bookServiceCommonMessages) {
        this.bookServiceCommon = bookServiceCommon;
        this.bookServiceCommonMessages = bookServiceCommonMessages;
    }

    @GetMapping("/")
    public String bookStore(Model model) {
        model.addAttribute("books", bookServiceCommon.findAllBooks());
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfAllBooks());
        return "books";
    }

    @PostMapping("/findBookById")
    public String findBookById(Long id, Model model) {
        model.addAttribute("books", List.of(bookServiceCommon.findBookById(id)));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBookById(id));
        return "books";
    }

    @PostMapping("/findBookByAuthor")
    public String findBookByAuthor(String author, Model model) {
        model.addAttribute("books", bookServiceCommon.findBookByAuthor(author));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBookByAuthor(author));
        return "books";
    }

    @PostMapping("/findBookByBookName")
    public String findBookByBookName(String bookName, Model model) {
        model.addAttribute("books", bookServiceCommon.findBookByBookName(bookName));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBookByBookName(bookName));
        return "books";
    }

    @PostMapping("/findBookByKeyword")
    public String findBookByKeyword(String keyword, Model model) {
        model.addAttribute("books", bookServiceCommon.findBookByKeyword(keyword));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBookByKeyword(keyword));
        return "books";
    }

    @PostMapping("/findBookByLessMaxPrice")
    public String findBookByPriceLowerMaxPrice(Integer maxPrice, Model model) {
        model.addAttribute("books", bookServiceCommon.findBooksByPriceLowerMax(maxPrice));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBooksByPriceLowerMax(maxPrice));
        return "books";
    }

    @PostMapping("/findBookByMoreMinPrice")
    public String findBookByPriceAboveMinPrice(Integer minPrice, Model model) {
        model.addAttribute("books", bookServiceCommon.findBooksByPriceAboveMin(minPrice));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfBooksByPriceAboveMin(minPrice));
        return "books";
    }

    @PostMapping("/findAllBooks")
    public String findAllBooks(Model model) {
        model.addAttribute("books", bookServiceCommon.findAllBooks());
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfAllBooks());
        return "books";
    }

    @PostMapping("/addBookToCart")
    public String addBookToCart(Long id, Model model) {

        model.addAttribute("books", bookServiceCommon.addBookToCart(id));
        model.addAttribute("message", bookServiceCommonMessages.getMessageOfAddBookToCart(id));
        return "books";
    }

    @ExceptionHandler(NoSuchBookException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/noSuchBook")
    public String onNoSuchBookError(NoSuchBookException e, Model model) {

        model.addAttribute("books", bookServiceCommon.findBookByAuthor(""));
        model.addAttribute("message", e.getMessage());
        return "books";
    }

    @ExceptionHandler(BookAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping("/error/bookAlreadyExists")
    public String onBookAlreadyExistsError(BookAlreadyExistsException e, Model model) {

        model.addAttribute("books", bookServiceCommon.findAllBooks());
        model.addAttribute("message", e.getMessage());
        return "books";
    }
}
