package com.example.bookshopdemo.error;

public class BookNotExistsInCartException extends RuntimeException{

    public BookNotExistsInCartException(String message) {
        super(message);
    }
}
