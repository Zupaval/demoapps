package com.example.bookshopdemo.error;

public class NoSuchCartException extends RuntimeException{

    public NoSuchCartException(String message) {
        super(message);
    }
}
