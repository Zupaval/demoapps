package com.example.bookshopdemo.error;

public class NoSuchBookException extends RuntimeException{

    public NoSuchBookException(String message) {
        super(message);
    }
}
