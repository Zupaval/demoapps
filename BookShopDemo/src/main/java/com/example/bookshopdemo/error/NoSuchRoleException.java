package com.example.bookshopdemo.error;

public class NoSuchRoleException extends RuntimeException{

    public NoSuchRoleException(String message) {
        super(message);
    }
}
