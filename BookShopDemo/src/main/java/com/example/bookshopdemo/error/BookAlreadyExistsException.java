package com.example.bookshopdemo.error;

public class BookAlreadyExistsException extends RuntimeException{

    public BookAlreadyExistsException(String message) {
        super(message);
    }
}
