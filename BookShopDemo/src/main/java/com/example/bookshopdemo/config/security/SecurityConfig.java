package com.example.bookshopdemo.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    public void authConfigure(
            AuthenticationManagerBuilder auth,
            PasswordEncoder encoder,
            UserAuthService userAuthService
    ) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin@mail.com")
                .password(encoder.encode("admin"))
                .roles("ADMIN");

        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userAuthService);
        provider.setPasswordEncoder(encoder);
        auth.authenticationProvider(provider);
    }

    String[] resources = new String[]{
            "/", "/static/images/**", "/static/CSS/**", "/templates/error/**"
    };

    String[] resourcesForUserAndAdmin = new String[]{
            "/books/**", "/bookStore", "/profile/**", "/cart/**", "/error/**"
    };

    String[] resourcesForAdmin = new String[]{
            "/moderators/**", "profileModerators/**", "bookModerators/**", "cartModerators/**"
    };

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(resources)
                .permitAll()
                .antMatchers(resourcesForUserAndAdmin)
                .hasAnyRole("USER", "ADMIN")
                .antMatchers(resourcesForAdmin)
                .hasAnyRole("ADMIN")
                .antMatchers("/login/**")
                .anonymous()
                .antMatchers("/registration/**")
                .anonymous()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll()
                .loginPage("/login")
                .usernameParameter("email")
                .passwordParameter("password")
                .defaultSuccessUrl("/bookStore")
                .failureUrl("/login/error")
                .and()
                .logout()
                .logoutUrl("/logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/login")
                .and()
                .httpBasic()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
        return http.build();
    }
}
