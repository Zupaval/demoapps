package com.example.bookshopdemo.service.implementations.moderators.messages;

import com.example.bookshopdemo.model.dto.BookDto;
import com.example.bookshopdemo.model.dto.mapper.BookMapper;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.repository.BookRepository;
import com.example.bookshopdemo.service.implementations.common.messages.BookServiceCommonMessagesImpl;
import com.example.bookshopdemo.service.interfaces.moderators.messages.BookServiceModeratorsMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookServiceModeratorsMessagesImpl extends BookServiceCommonMessagesImpl
        implements BookServiceModeratorsMessages {

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceModeratorsMessagesImpl(BookRepository bookRepository) {
        super(bookRepository);
        this.bookRepository = bookRepository;
    }

    @Transactional
    @Override
    public String getMessageOfSaveBook(BookDto bookDto) {

        BookEntity bookEntity = BookMapper.fromDtoToEntity(bookDto);
        BookEntity bookByAuthorAndBookName = bookRepository
                .findBookByAuthorAndBookName(bookEntity.getAuthor(), bookEntity.getBookName());

        return "Book by id: " + bookByAuthorAndBookName.getId() + " successfully saved";
    }

    @Transactional
    @Override
    public String getMessageOfUpdateBook(BookDto bookDto) {

        return "Book by id: " + bookDto.getId() + " successfully updated";
    }

    @Transactional
    @Override
    public String getMessageOfDeleteBook(Long id) {
        return "Book by id: " + id + " successfully deleted";
    }
}
