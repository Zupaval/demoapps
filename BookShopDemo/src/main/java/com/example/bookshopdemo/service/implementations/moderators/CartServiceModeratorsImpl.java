package com.example.bookshopdemo.service.implementations.moderators;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.BookNotExistsInCartException;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.error.NoSuchCartException;
import com.example.bookshopdemo.model.dto.CartDto;
import com.example.bookshopdemo.model.dto.mapper.CartMapper;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.entity.CartEntity;
import com.example.bookshopdemo.model.repository.BookRepository;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.service.implementations.common.CartServiceCommonImpl;
import com.example.bookshopdemo.service.interfaces.moderators.CartServiceModerators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartServiceModeratorsImpl extends CartServiceCommonImpl implements CartServiceModerators {

    private final CartRepository cartRepository;

    private final BookRepository bookRepository;

    @Autowired
    public CartServiceModeratorsImpl(CartRepository cartRepository, BookRepository bookRepository,
                                     CurrentUserId currentUserId) {

        super(cartRepository, bookRepository, currentUserId);
        this.cartRepository = cartRepository;
        this.bookRepository = bookRepository;
    }

    @Transactional
    @Override
    public CartDto findCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public Long totalQuantityOfCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();
    }

    @Transactional
    @Override
    public Integer totalCoastOfCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getPrice)
                .reduce(Integer::sum).orElse(0);
    }

    @Transactional
    @Override
    public List<CartDto> findAllCarts() {

        return cartRepository.findAll()
                .stream()
                .map(CartMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public Long totalQuantityOfBooksOfAllCarts() {

        List<CartEntity> carts = cartRepository.findAll();
        return carts
                .stream()
                .map(CartEntity::getBooks)
                .map(books -> books.stream()
                        .map(BookEntity::getId))
                .count();
    }

    @Transactional
    @Override
    public Long totalCoastOfBooksOfAllCarts() {

        List<CartEntity> carts = cartRepository.findAll();
        return carts
                .stream()
                .map(CartEntity::getBooks)
                .map(books -> books.stream()
                        .map(BookEntity::getPrice)
                        .map(Integer::toUnsignedLong)
                        .reduce(Long::sum).orElse(0L))
                .reduce(Long::sum).orElse(0L);
    }

    @Transactional
    @Override
    public CartDto addBookToCartById(Long cartId, Long id) {

        CartEntity cartEntityTemp = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        List<BookEntity> books = cartEntityTemp.getBooks();
        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        books.add(bookEntity);
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto deleteBookFromCartById(Long cartId, Long id) {

        CartEntity cartEntityTemp = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        List<BookEntity> books = cartEntityTemp.getBooks();
        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        if (!books.contains(bookRepository.findById(id).orElseThrow())) {
            throw new BookNotExistsInCartException("Book by id: " + id + " not exists in cart by id: " + cartId);
        }

        books.remove(bookEntity);
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto deleteAllBooksFromCartById(Long cartId) {

        CartEntity cartEntityTemp = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        List<BookEntity> books = cartEntityTemp.getBooks();
        books.clear();
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }
}
