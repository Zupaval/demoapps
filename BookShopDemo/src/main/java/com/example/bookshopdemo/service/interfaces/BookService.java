package com.example.bookshopdemo.service.interfaces;

import com.example.bookshopdemo.model.dto.BookDto;

import java.util.List;

public interface BookService {

    BookDto findBookById(Long id);

    List<BookDto> findBookByAuthor(String author);

    List<BookDto> findBookByBookName(String bookName);

    List<BookDto> findAllBooks();

    List<BookDto> findBooksByPriceLowerMax(Integer maxPrice);

    List<BookDto> findBooksByPriceAboveMin(Integer minPrice);

    List<BookDto> findBookByKeyword(String keyword);

    BookDto saveBook(BookDto bookDto);

    BookDto updateBook(BookDto bookDto);

    BookDto deleteBookById(Long id);

    List<BookDto> addBookToCart(Long id);

    String getMessageOfAllBooks();

    String getMessageOfBookById(Long id);

    String getMessageOfBookByAuthor(String author);

    String getMessageOfBookByBookName(String bookName);

    String getMessageOfBookByKeyword(String keyword);

    String getMessageOfBooksByPriceLowerMax(Integer maxPrice);

    String getMessageOfBooksByPriceAboveMin(Integer minPrice);

    String getMessageOfAddBookToCart(Long id);

    String getMessageOfSaveBook(BookDto bookDto);

    String getMessageOfUpdateBook(BookDto bookDto);

    String getMessageOfDeleteBook(Long id);
}