package com.example.bookshopdemo.service.implementations;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.BookNotExistsInCartException;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.error.NoSuchCartException;
import com.example.bookshopdemo.model.dto.CartDto;
import com.example.bookshopdemo.model.dto.mapper.CartMapper;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.entity.CartEntity;
import com.example.bookshopdemo.model.repository.BookRepository;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.service.interfaces.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;

    private final BookRepository bookRepository;

    private final CurrentUserId currentUserId;


    @Autowired
    public CartServiceImpl(CartRepository cartRepository, BookRepository bookRepository, CurrentUserId currentUserId) {
        this.cartRepository = cartRepository;
        this.bookRepository = bookRepository;
        this.currentUserId = currentUserId;
    }

    @Transactional
    @Override
    public CartDto findCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto findCart() {
        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public Integer totalCoastOfCart() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getPrice)
                .reduce(Integer::sum).orElse(0);
    }

    @Transactional
    @Override
    public Integer totalCoastOfCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getPrice)
                .reduce(Integer::sum).orElse(0);
    }

    @Transactional
    @Override
    public Integer totalCoastOfBooksOfAllCarts() {

        List<CartEntity> carts = cartRepository.findAll();
        return carts
                .stream()
                .map(CartEntity::getBooks)
                .map(books -> books.stream()
                        .map(BookEntity::getPrice).reduce(Integer::sum).orElse(0))
                .reduce(Integer::sum).orElse(0);
    }


    @Transactional
    @Override
    public Long totalQuantityOfCart() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();
    }

    @Transactional
    @Override
    public Long totalQuantityOfCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();
    }

    @Transactional
    @Override
    public Long totalQuantityOfBooksOfAllCarts() {

        List<CartEntity> carts = cartRepository.findAll();
        return carts
                .stream()
                .map(CartEntity::getBooks)
                .map(books -> books.stream()
                        .map(BookEntity::getId))
                .count();
    }

    @Transactional
    @Override
    public CartDto addBookToCart(Long id) {

        CartEntity cartEntityTemp = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        List<BookEntity> books = cartEntityTemp.getBooks();
        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        books.add(bookEntity);
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto addBookToCartById(Long cartId, Long id) {

        CartEntity cartEntityTemp = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        List<BookEntity> books = cartEntityTemp.getBooks();
        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        books.add(bookEntity);
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto deleteBookFromCart(Long id) {

        CartEntity cartEntityTemp = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        List<BookEntity> books = cartEntityTemp.getBooks();
        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        books.remove(bookEntity);
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto deleteBookFromCartById(Long cartId, Long id) {

        CartEntity cartEntityTemp = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        List<BookEntity> books = cartEntityTemp.getBooks();
        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        if (!books.contains(bookRepository.findById(id).orElseThrow())) {
            throw new BookNotExistsInCartException("Book by id: " + id + " not exists in cart by id: " + cartId);
        }

        books.remove(bookEntity);
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto deleteAllBooksFromCart() {

        CartEntity cartEntityTemp = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        List<BookEntity> books = cartEntityTemp.getBooks();
        books.clear();
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto deleteAllBooksFromCartById(Long cartId) {

        CartEntity cartEntityTemp = cartRepository.findById(cartId)
                .orElseThrow(() -> new NoSuchCartException("Cart by id: " + cartId + " not found!"));
        List<BookEntity> books = cartEntityTemp.getBooks();
        books.clear();
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public List<CartDto> findAllCarts() {

        return cartRepository.findAll()
                .stream()
                .map(CartMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public String messageOfMyCart() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        long quantity = cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();

        if (quantity == 0) return "Your cart without books";
        if (quantity == 1) return "Your cart with 1 book";
        return "Your cart with " + quantity + " books";
    }

    @Transactional
    @Override
    public String messageOfCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId).orElseThrow();
        long quantity = cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();

        if (quantity == 0) return "Cart by id: " + cartId + " without books";
        if (quantity == 1) return "Cart by id: " + cartId + " with 1 book";
        return "Cart by id: " + cartId + " with " + quantity + " books";
    }

    @Transactional
    @Override
    public String messageOfAllCarts() {

        List<CartEntity> carts = cartRepository.findAll();
        long quantity = carts
                .stream()
                .map(CartEntity::getBooks)
                .map(books -> books.stream()
                        .map(BookEntity::getId))
                .count();

        if (quantity == 0) return "All carts without books";
        if (quantity == 1) return "All carts with 1 book";
        return "All carts with " + quantity + " books";
    }

    @Transactional
    @Override
    public String messageOfAddBookToMyCart(Long id) {

        return "Book by id: " + id + "successfully added to your cart";
    }

    @Transactional
    @Override
    public String messageOfAddBookToCartById(Long cartId, Long id) {

        return "Book by id: " + id + "successfully added to cart by id: " + cartId;
    }

    @Transactional
    @Override
    public String messageOfDeleteBookFromMyCart(Long id) {

        return "Book by id: " + id + "successfully deleted from your cart";
    }

    @Transactional
    @Override
    public String messageOfDeleteBookFromCartById(Long cartId, Long id) {

        return "Book by id: " + id + "successfully deleted from cart by id: " + cartId;
    }

    @Transactional
    @Override
    public String messageOfDeleteAllBooksFromMyCart() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();

        long quantity = cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();

        if (quantity == 0) return "No books in your cart";
        if (quantity == 1) return "1 book successfully deleted from your cart";
        return quantity + " books successfully deleted from from your cart";
    }

    @Transactional
    @Override
    public String messageOfDeleteAllBooksFromCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId).orElseThrow();
        long quantity = cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();

        if (quantity == 0) return "No books in cart by id: " + cartId;
        if (quantity == 1) return "1 book successfully deleted from cart by id: " + cartId;
        return quantity + " books successfully deleted from cart by id: " + cartId;
    }
}
