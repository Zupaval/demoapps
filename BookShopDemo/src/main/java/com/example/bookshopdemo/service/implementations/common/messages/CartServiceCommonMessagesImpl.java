package com.example.bookshopdemo.service.implementations.common.messages;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.entity.CartEntity;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.service.interfaces.common.messages.CartServiceCommonMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CartServiceCommonMessagesImpl implements CartServiceCommonMessages {

    private final CartRepository cartRepository;

    private final CurrentUserId currentUserId;

    @Autowired
    public CartServiceCommonMessagesImpl(CartRepository cartRepository, CurrentUserId currentUserId) {
        this.cartRepository = cartRepository;
        this.currentUserId = currentUserId;
    }

    @Transactional
    @Override
    public String messageOfMyCart() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        long quantity = cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();

        if (quantity == 0) return "Your cart without books";
        if (quantity == 1) return "Your cart with 1 book";
        return "Your cart with " + quantity + " books";
    }

    @Transactional
    @Override
    public String messageOfAddBookToMyCart(Long id) {

        return "Book by id: " + id + "successfully added to your cart";
    }

    @Transactional
    @Override
    public String messageOfDeleteBookFromMyCart(Long id) {

        return "Book by id: " + id + "successfully deleted from your cart";
    }

    @Transactional
    @Override
    public String messageOfDeleteAllBooksFromMyCart() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();

        long quantity = cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();

        if (quantity == 0) return "No books in your cart";
        if (quantity == 1) return "1 book successfully deleted from your cart";
        return quantity + " books successfully deleted from from your cart";
    }
}
