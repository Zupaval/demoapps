package com.example.bookshopdemo.service.interfaces;

import com.example.bookshopdemo.model.dto.CartDto;

import java.util.List;

public interface CartService {

    CartDto findCartById(Long cartId);

    CartDto findCart();

    Integer totalCoastOfCart();

    Integer totalCoastOfCartById(Long cartId);

    Long totalQuantityOfCart();

    Long totalQuantityOfCartById(Long cartId);

    CartDto addBookToCart(Long id);

    CartDto addBookToCartById(Long cartId, Long id);

    CartDto deleteBookFromCart(Long id);

    CartDto deleteBookFromCartById(Long cartId, Long id);

    CartDto deleteAllBooksFromCart();

    CartDto deleteAllBooksFromCartById(Long cartId);

    List<CartDto> findAllCarts();

    Long totalQuantityOfBooksOfAllCarts();

    Integer totalCoastOfBooksOfAllCarts();

    String messageOfMyCart();

    String messageOfCartById(Long cartId);

    String messageOfAllCarts();

    String messageOfAddBookToMyCart(Long id);

    String messageOfAddBookToCartById(Long cartId, Long id);

    String messageOfDeleteBookFromMyCart(Long id);

    String messageOfDeleteBookFromCartById(Long cartId, Long id);

    String messageOfDeleteAllBooksFromMyCart();

    String messageOfDeleteAllBooksFromCartById(Long cartId);
}
