package com.example.bookshopdemo.service.implementations.common;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.model.dto.CartDto;
import com.example.bookshopdemo.model.dto.mapper.CartMapper;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.entity.CartEntity;
import com.example.bookshopdemo.model.repository.BookRepository;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.service.interfaces.common.CartServiceCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CartServiceCommonImpl implements CartServiceCommon {

    private final CartRepository cartRepository;

    private final BookRepository bookRepository;

    private final CurrentUserId currentUserId;

    @Autowired
    public CartServiceCommonImpl(CartRepository cartRepository, BookRepository bookRepository, CurrentUserId currentUserId) {
        this.cartRepository = cartRepository;
        this.bookRepository = bookRepository;
        this.currentUserId = currentUserId;
    }

    @Transactional
    @Override
    public CartDto findCart() {
        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public Long totalQuantityOfCart() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();
    }

    @Transactional
    @Override
    public Integer totalCoastOfCart() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getPrice)
                .reduce(Integer::sum).orElse(0);
    }

    @Transactional
    @Override
    public CartDto addBookToCart(Long id) {

        CartEntity cartEntityTemp = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        List<BookEntity> books = cartEntityTemp.getBooks();
        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        books.add(bookEntity);
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto deleteBookFromCart(Long id) {

        CartEntity cartEntityTemp = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        List<BookEntity> books = cartEntityTemp.getBooks();
        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        books.remove(bookEntity);
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public CartDto deleteAllBooksFromCart() {

        CartEntity cartEntityTemp = cartRepository.findById(currentUserId.getUserId()).orElseThrow();
        List<BookEntity> books = cartEntityTemp.getBooks();
        books.clear();
        cartEntityTemp.setBooks(books);
        CartEntity cartEntity = cartRepository.saveAndFlush(cartEntityTemp);
        return CartMapper.fromEntityToDto(cartEntity);
    }
}
