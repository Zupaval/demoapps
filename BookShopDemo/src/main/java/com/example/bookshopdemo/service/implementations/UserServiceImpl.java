package com.example.bookshopdemo.service.implementations;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.EmailAlreadyExistsException;
import com.example.bookshopdemo.error.NoSuchRoleException;
import com.example.bookshopdemo.error.NoSuchUserException;
import com.example.bookshopdemo.model.dto.UserDto;
import com.example.bookshopdemo.model.dto.mapper.UserMapper;
import com.example.bookshopdemo.model.entity.CartEntity;
import com.example.bookshopdemo.model.entity.RoleEntity;
import com.example.bookshopdemo.model.entity.UserEntity;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.model.repository.RoleRepository;
import com.example.bookshopdemo.model.repository.UserRepository;
import com.example.bookshopdemo.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final CurrentUserId currentUserId;

    private final CartRepository cartRepository;

    private final RoleRepository roleRepository;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, CurrentUserId currentUserId,
                           CartRepository cartRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.currentUserId = currentUserId;
        this.cartRepository = cartRepository;
        this.roleRepository = roleRepository;
    }

    @Transactional
    @Override
    public UserDto findCurrentUser() {

        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto findUserById(Long id) {

        UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(() -> new NoSuchUserException("User by id: " + id + " not found!"));
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto findUserByEmail(String email) {

        UserEntity userEntity = userRepository.findUserByEmail(email)
                .orElseThrow(() -> new NoSuchUserException("User by email: " + email + " not found!"));
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public List<UserDto> findUserByLastName(String lastName) {

        return userRepository.findAllUserByLastName(lastName)
                .stream()
                .map(UserMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<UserDto> findAllUsers() {

        return userRepository.findAll()
                .stream()
                .map(UserMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public UserDto addUserRole(Long UserId, Long RoleId) {

        userRepository.findById(UserId)
                .orElseThrow(() -> new NoSuchUserException("User by id: " + UserId + " not found!"));
        roleRepository.findById(RoleId)
                        .orElseThrow(() -> new NoSuchRoleException("Role by id: " + RoleId + " not found!"));
        userRepository.addUserRole(UserId, RoleId);
        UserEntity userEntity = userRepository.findById(UserId).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto deleteUserRole(Long UserId, Long RoleId) {

        userRepository.findById(UserId)
                .orElseThrow(() -> new NoSuchUserException("User by id: " + UserId + " not found!"));
        roleRepository.findById(RoleId)
                .orElseThrow(() -> new NoSuchRoleException("Role by id: " + RoleId + " not found!"));
        userRepository.removeUserRole(UserId, RoleId);
        UserEntity userEntity = userRepository.findById(UserId).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public void saveUser(UserDto userDto) {


        List<String> userEmails = userRepository.findAll()
                .stream()
                .map(UserMapper::fromEntityToDto)
                .map(UserDto::getEmail)
                .collect(Collectors.toList());

        if(userEmails.contains(userDto.getEmail())) {

            throw new EmailAlreadyExistsException("User with email: " + userDto.getEmail() + " already exists!");
        }

        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        UserEntity userEntityTemp = UserMapper.fromDtoToEntity(userDto);
        userEntityTemp.setRoles(List.of(new RoleEntity(2L, "USER")));
        UserEntity userEntity = userRepository.save(userEntityTemp);
        CartEntity cartEntity = new CartEntity();
        cartEntity.setId(userEntity.getId());
        cartRepository.save(cartEntity);
    }

    @Transactional
    @Override
    public UserDto updateUser(UserDto userDto) {

        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        userDto.setId(userEntity.getId());
        userDto.setEmail(userEntity.getEmail());
        userDto.setRoles(userEntity.getRoles());
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userEntity = userRepository.saveAndFlush(UserMapper.fromDtoToEntity(userDto));
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto deleteUserById(Long id) {

        userRepository.findById(id)
                .orElseThrow(() -> new NoSuchUserException("User by id: " + id + " not found!"));

        UserEntity userEntity = userRepository.deleteUserById(id);

        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public String getMessageOfCurrentUser() {
        return "Your current profile";
    }

    @Transactional
    @Override
    public String getMessageOfUpdateUser() {
        return "Your current profile updated successfully";
    }

    @Transactional
    @Override
    public String getMessageOfUserById(Long id) {

        return "User by id: " + id + " found";
    }

    @Transactional
    @Override
    public String getMessageOfUserByEmail(String email) {

        return "User by email: " + email + " found";
    }

    @Transactional
    @Override
    public String getMessageOfUserByLastName(String lastName) {

        int quantity = userRepository.findAllUserByLastName(lastName).size();

        if (quantity == 0) return "No users found";
        if (quantity == 1) return "1 user found";
        return quantity + " users found";
    }

    @Transactional
    @Override
    public String getMessageOfAllUsers() {

        int quantity = userRepository.findAll().size();

        if (quantity == 0) return "No users found";
        if (quantity == 1) return "1 user found";
        return quantity + " users found";
    }

    @Transactional
    @Override
    public String getMessageOfAddUserRole(Long userId, Long roleId) {

        return "User by id: " + userId + " successfully added role by id: " + roleId;
    }

    @Transactional
    @Override
    public String getMessageOfDeleteUserRole(Long userId, Long roleId) {

        return "User by id: " + userId + " successfully deleted role by id: " + roleId;
    }

    @Transactional
    @Override
    public String getMessageOfDeleteUserById(Long id) {

        return "User by id: " + id + " successfully deleted";
    }
}
