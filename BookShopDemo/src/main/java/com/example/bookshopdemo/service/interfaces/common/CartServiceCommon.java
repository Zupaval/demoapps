package com.example.bookshopdemo.service.interfaces.common;

import com.example.bookshopdemo.model.dto.CartDto;

public interface CartServiceCommon {

    CartDto findCart();

    Long totalQuantityOfCart();

    Integer totalCoastOfCart();

    CartDto addBookToCart(Long id);

    CartDto deleteBookFromCart(Long id);

    CartDto deleteAllBooksFromCart();
}
