package com.example.bookshopdemo.service.implementations.common;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.EmailAlreadyExistsException;
import com.example.bookshopdemo.model.dto.UserDto;
import com.example.bookshopdemo.model.dto.mapper.UserMapper;
import com.example.bookshopdemo.model.entity.CartEntity;
import com.example.bookshopdemo.model.entity.RoleEntity;
import com.example.bookshopdemo.model.entity.UserEntity;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.model.repository.UserRepository;
import com.example.bookshopdemo.service.interfaces.common.UserServiceCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceCommonImpl implements UserServiceCommon {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final CurrentUserId currentUserId;

    private final CartRepository cartRepository;

    @Autowired
    public UserServiceCommonImpl(UserRepository userRepository, PasswordEncoder passwordEncoder,
                                 CurrentUserId currentUserId, CartRepository cartRepository) {

        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.currentUserId = currentUserId;
        this.cartRepository = cartRepository;
    }

    @Transactional
    @Override
    public UserDto findCurrentUser() {

        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }


    @Transactional
    @Override
    public UserDto updateUser(UserDto userDto) {

        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        userDto.setId(userEntity.getId());
        userDto.setEmail(userEntity.getEmail());
        userDto.setRoles(userEntity.getRoles());
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userEntity = userRepository.saveAndFlush(UserMapper.fromDtoToEntity(userDto));
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public void saveUser(UserDto userDto) {

        List<String> userEmails = userRepository.findAll()
                .stream()
                .map(UserEntity::getEmail)
                .collect(Collectors.toList());

        if (userEmails.contains(userDto.getEmail())) {
            throw new EmailAlreadyExistsException("User with email: " + userDto.getEmail() + " already exists!");
        }

        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        UserEntity userEntityTemp = UserMapper.fromDtoToEntity(userDto);
        userEntityTemp.setRoles(List.of(new RoleEntity(2L, "USER")));
        UserEntity userEntity = userRepository.save(userEntityTemp);
        CartEntity cartEntity = new CartEntity();
        cartEntity.setId(userEntity.getId());
        cartRepository.save(cartEntity);
    }
}
