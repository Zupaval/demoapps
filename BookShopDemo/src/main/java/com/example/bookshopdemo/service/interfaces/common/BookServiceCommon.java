package com.example.bookshopdemo.service.interfaces.common;

import com.example.bookshopdemo.model.dto.BookDto;

import java.util.List;

public interface BookServiceCommon {

    List<BookDto> findAllBooks();

    BookDto findBookById(Long id);

    List<BookDto> findBookByAuthor(String author);

    List<BookDto> findBookByBookName(String bookName);

    List<BookDto> findBookByKeyword(String keyword);

    List<BookDto> findBooksByPriceLowerMax(Integer maxPrice);

    List<BookDto> findBooksByPriceAboveMin(Integer minPrice);

    List<BookDto> addBookToCart(Long id);
}
