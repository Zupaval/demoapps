package com.example.bookshopdemo.service.interfaces.moderators;

import com.example.bookshopdemo.model.dto.UserDto;
import com.example.bookshopdemo.service.interfaces.common.UserServiceCommon;

import java.util.List;

public interface UserServiceModerators extends UserServiceCommon {

    UserDto findUserById(Long id);

    UserDto findUserByEmail(String email);

    List<UserDto> findUserByLastName(String lastName);

    List<UserDto> findAllUsers();

    UserDto addUserRole(Long UserId, Long RoleId);

    UserDto deleteUserRole(Long UserId, Long RoleId);

    UserDto deleteUserById(Long id);
}
