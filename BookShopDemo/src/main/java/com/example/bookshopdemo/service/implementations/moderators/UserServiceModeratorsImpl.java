package com.example.bookshopdemo.service.implementations.moderators;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.NoSuchRoleException;
import com.example.bookshopdemo.error.NoSuchUserException;
import com.example.bookshopdemo.model.dto.UserDto;
import com.example.bookshopdemo.model.dto.mapper.UserMapper;
import com.example.bookshopdemo.model.entity.UserEntity;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.model.repository.RoleRepository;
import com.example.bookshopdemo.model.repository.UserRepository;
import com.example.bookshopdemo.service.implementations.common.UserServiceCommonImpl;
import com.example.bookshopdemo.service.interfaces.moderators.UserServiceModerators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceModeratorsImpl extends UserServiceCommonImpl implements UserServiceModerators {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceModeratorsImpl(UserRepository userRepository, PasswordEncoder passwordEncoder,
                                     CurrentUserId currentUserId, CartRepository cartRepository,
                                     RoleRepository roleRepository) {
        super(userRepository, passwordEncoder, currentUserId, cartRepository);
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Transactional
    @Override
    public UserDto findUserById(Long id) {

        UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(() -> new NoSuchUserException("User by id: " + id + " not found!"));
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto findUserByEmail(String email) {

        UserEntity userEntity = userRepository.findUserByEmail(email)
                .orElseThrow(() -> new NoSuchUserException("User by email: " + email + " not found!"));
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public List<UserDto> findUserByLastName(String lastName) {

        return userRepository.findAllUserByLastName(lastName)
                .stream()
                .map(UserMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<UserDto> findAllUsers() {

        return userRepository.findAll()
                .stream()
                .map(UserMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public UserDto addUserRole(Long UserId, Long RoleId) {

        userRepository.findById(UserId)
                .orElseThrow(() -> new NoSuchUserException("User by id: " + UserId + " not found!"));
        roleRepository.findById(RoleId)
                .orElseThrow(() -> new NoSuchRoleException("Role by id: " + RoleId + " not found!"));
        userRepository.addUserRole(UserId, RoleId);
        UserEntity userEntity = userRepository.findById(UserId).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto deleteUserRole(Long UserId, Long RoleId) {

        userRepository.findById(UserId)
                .orElseThrow(() -> new NoSuchUserException("User by id: " + UserId + " not found!"));
        roleRepository.findById(RoleId)
                .orElseThrow(() -> new NoSuchRoleException("Role by id: " + RoleId + " not found!"));
        userRepository.removeUserRole(UserId, RoleId);
        UserEntity userEntity = userRepository.findById(UserId).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto deleteUserById(Long id) {

        userRepository.findById(id)
                .orElseThrow(() -> new NoSuchUserException("User by id: " + id + " not found!"));

        UserEntity userEntity = userRepository.deleteUserById(id);
        return UserMapper.fromEntityToDto(userEntity);
    }
}
