package com.example.bookshopdemo.service.interfaces.moderators;

import com.example.bookshopdemo.model.dto.CartDto;
import com.example.bookshopdemo.service.interfaces.common.CartServiceCommon;

import java.util.List;

public interface CartServiceModerators extends CartServiceCommon {

    CartDto findCartById(Long cartId);

    Long totalQuantityOfCartById(Long cartId);

    Integer totalCoastOfCartById(Long cartId);

    List<CartDto> findAllCarts();

    Long totalQuantityOfBooksOfAllCarts();

    Long totalCoastOfBooksOfAllCarts();

    CartDto addBookToCartById(Long cartId, Long id);

    CartDto deleteBookFromCartById(Long cartId, Long id);

    CartDto deleteAllBooksFromCartById(Long cartId);
}
