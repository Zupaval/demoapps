package com.example.bookshopdemo.service.interfaces.moderators;

import com.example.bookshopdemo.model.dto.BookDto;
import com.example.bookshopdemo.service.interfaces.common.BookServiceCommon;

public interface BookServiceModerators extends BookServiceCommon {

    BookDto saveBook(BookDto bookDto);

    BookDto updateBook(BookDto bookDto);

    BookDto deleteBookById(Long id);
}
