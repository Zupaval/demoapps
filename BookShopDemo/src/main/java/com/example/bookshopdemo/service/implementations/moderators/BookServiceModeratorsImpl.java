package com.example.bookshopdemo.service.implementations.moderators;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.BookAlreadyExistsException;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.model.dto.BookDto;
import com.example.bookshopdemo.model.dto.mapper.BookMapper;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.repository.BookRepository;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.service.implementations.common.BookServiceCommonImpl;
import com.example.bookshopdemo.service.interfaces.moderators.BookServiceModerators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class BookServiceModeratorsImpl extends BookServiceCommonImpl implements BookServiceModerators {

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceModeratorsImpl(BookRepository bookRepository, CartRepository cartRepository,
                                     CurrentUserId currentUserId) {
        super(bookRepository, cartRepository, currentUserId);
        this.bookRepository = bookRepository;
    }

    @Transactional
    @Override
    public BookDto saveBook(BookDto bookDto) {

        BookEntity bookEntityTemp = BookMapper.fromDtoToEntity(bookDto);
        BookEntity bookByAuthorAndBookName = bookRepository
                .findBookByAuthorAndBookName(bookEntityTemp.getAuthor(), bookEntityTemp.getBookName());

        if (bookByAuthorAndBookName != null) {
            throw new BookAlreadyExistsException("Book already exists by id: " + bookByAuthorAndBookName.getId());
        }

        BookEntity bookEntity = bookRepository.save(BookMapper.fromDtoToEntity(bookDto));
        bookDto.setId(bookEntity.getId());
        return bookDto;
    }

    @Transactional
    @Override
    public BookDto updateBook(BookDto bookDto) {

        bookRepository.findById(bookDto.getId())
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + bookDto.getId() + " not found!"));

        BookEntity bookEntityTemp = BookMapper.fromDtoToEntity(bookDto);
        BookEntity bookByAuthorAndBookName = bookRepository
                .findBookByAuthorAndBookName(bookEntityTemp.getAuthor(), bookEntityTemp.getBookName());

        if (bookByAuthorAndBookName != null) {
            if (!Objects.equals(bookByAuthorAndBookName.getId(), bookDto.getId())) {
                throw new BookAlreadyExistsException("Book with same author and name already exists by id: "
                        + bookByAuthorAndBookName.getId());
            }
        }
        BookEntity bookEntity = bookRepository.saveAndFlush(BookMapper.fromDtoToEntity(bookDto));
        return BookMapper.fromEntityToDto(bookEntity);
    }

    @Transactional
    @Override
    public BookDto deleteBookById(Long id) {

        bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));

        BookEntity bookEntity = bookRepository.deleteBookById(id);
        return BookMapper.fromEntityToDto(bookEntity);
    }
}
