package com.example.bookshopdemo.service.interfaces.common.messages;

public interface CartServiceCommonMessages {

    String messageOfMyCart();

    String messageOfAddBookToMyCart(Long id);

    String messageOfDeleteBookFromMyCart(Long id);

    String messageOfDeleteAllBooksFromMyCart();
}
