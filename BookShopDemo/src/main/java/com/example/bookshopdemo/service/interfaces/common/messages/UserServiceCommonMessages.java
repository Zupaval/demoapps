package com.example.bookshopdemo.service.interfaces.common.messages;

public interface UserServiceCommonMessages {

    String getMessageOfCurrentUser();

    String getMessageOfUpdateUser();
}
