package com.example.bookshopdemo.service.implementations.common;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.model.dto.BookDto;
import com.example.bookshopdemo.model.dto.mapper.BookMapper;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.entity.CartEntity;
import com.example.bookshopdemo.model.repository.BookRepository;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.service.interfaces.common.BookServiceCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceCommonImpl implements BookServiceCommon {

    private final BookRepository bookRepository;

    private final CartRepository cartRepository;

    private final CurrentUserId currentUserId;

    @Autowired
    public BookServiceCommonImpl(BookRepository bookRepository, CartRepository cartRepository, CurrentUserId currentUserId) {
        this.bookRepository = bookRepository;
        this.cartRepository = cartRepository;
        this.currentUserId = currentUserId;
    }

    @Transactional
    @Override
    public List<BookDto> findAllBooks() {

        return bookRepository.findAll()
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public BookDto findBookById(Long id) {

        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        return BookMapper.fromEntityToDto(bookEntity);
    }

    @Transactional
    @Override
    public List<BookDto> findBookByAuthor(String author) {

        return bookRepository.findAllBookByAuthor(author)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBookByBookName(String bookName) {

        return bookRepository.findAllBookByBookName(bookName)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBookByKeyword(String keyword) {

        keyword = "%" + keyword + "%";
        return bookRepository.findBooksByKeyword(keyword)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBooksByPriceLowerMax(Integer maxPrice) {

        return bookRepository.findBooksByPriceLowerMax(maxPrice)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBooksByPriceAboveMin(Integer minPrice) {

        return bookRepository.findBooksByPriceAboveMin(minPrice)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> addBookToCart(Long id) {

        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));

        CartEntity cartEntityTemp = cartRepository.findById(currentUserId.getUserId()).orElseThrow();

        List<BookEntity> books = cartEntityTemp.getBooks();
        books.add(bookEntity);
        cartEntityTemp.setBooks(books);
        cartRepository.saveAndFlush(cartEntityTemp);

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();

        return cartEntity.getBooks()
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }
}
