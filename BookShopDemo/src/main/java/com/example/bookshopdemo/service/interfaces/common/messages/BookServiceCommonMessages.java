package com.example.bookshopdemo.service.interfaces.common.messages;

import com.example.bookshopdemo.model.dto.BookDto;

public interface BookServiceCommonMessages {

    String getMessageOfAllBooks();

    String getMessageOfBookById(Long id);

    String getMessageOfBookByAuthor(String author);

    String getMessageOfBookByBookName(String bookName);

    String getMessageOfBookByKeyword(String keyword);

    String getMessageOfBooksByPriceLowerMax(Integer maxPrice);

    String getMessageOfBooksByPriceAboveMin(Integer minPrice);

    String getMessageOfAddBookToCart(Long id);
}
