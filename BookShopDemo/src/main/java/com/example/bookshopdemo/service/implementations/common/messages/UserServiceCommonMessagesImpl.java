package com.example.bookshopdemo.service.implementations.common.messages;

import com.example.bookshopdemo.service.interfaces.common.messages.UserServiceCommonMessages;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceCommonMessagesImpl implements UserServiceCommonMessages {

    @Transactional
    @Override
    public String getMessageOfCurrentUser() {
        return "Your current profile";
    }

    @Transactional
    @Override
    public String getMessageOfUpdateUser() {
        return "Your current profile updated successfully";
    }
}
