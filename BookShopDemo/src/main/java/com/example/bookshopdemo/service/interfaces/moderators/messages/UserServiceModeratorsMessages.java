package com.example.bookshopdemo.service.interfaces.moderators.messages;

import com.example.bookshopdemo.service.interfaces.common.messages.UserServiceCommonMessages;

public interface UserServiceModeratorsMessages extends UserServiceCommonMessages {

    String getMessageOfUserById(Long id);

    String getMessageOfUserByEmail(String email);

    String getMessageOfUserByLastName(String lastName);

    String getMessageOfAllUsers();

    String getMessageOfAddUserRole(Long userId, Long roleId);

    String getMessageOfDeleteUserRole(Long userId, Long roleId);

    String getMessageOfDeleteUserById(Long id);
}
