package com.example.bookshopdemo.service.implementations.moderators.messages;

import com.example.bookshopdemo.model.repository.UserRepository;
import com.example.bookshopdemo.service.implementations.common.messages.UserServiceCommonMessagesImpl;
import com.example.bookshopdemo.service.interfaces.moderators.messages.UserServiceModeratorsMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceModeratorsMessagesImpl extends UserServiceCommonMessagesImpl
        implements UserServiceModeratorsMessages {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceModeratorsMessagesImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public String getMessageOfUserById(Long id) {
        return "User by id: " + id + " found";
    }

    @Transactional
    @Override
    public String getMessageOfUserByEmail(String email) {
        return "User by email: " + email + " found";
    }

    @Transactional
    @Override
    public String getMessageOfUserByLastName(String lastName) {

        int quantity = userRepository.findAllUserByLastName(lastName).size();
        if (quantity == 0) return "No users found";
        if (quantity == 1) return "1 user found";
        return quantity + " users found";
    }

    @Transactional
    @Override
    public String getMessageOfAllUsers() {

        int quantity = userRepository.findAll().size();
        if (quantity == 0) return "No users found";
        if (quantity == 1) return "1 user found";
        return quantity + " users found";
    }

    @Transactional
    @Override
    public String getMessageOfAddUserRole(Long userId, Long roleId) {
        return "User by id: " + userId + " successfully added role by id: " + roleId;
    }

    @Transactional
    @Override
    public String getMessageOfDeleteUserRole(Long userId, Long roleId) {
        return "User by id: " + userId + " successfully deleted role by id: " + roleId;
    }

    @Transactional
    @Override
    public String getMessageOfDeleteUserById(Long id) {
        return "User by id: " + id + " successfully deleted";
    }
}
