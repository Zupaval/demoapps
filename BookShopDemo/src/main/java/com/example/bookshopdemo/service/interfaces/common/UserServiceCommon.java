package com.example.bookshopdemo.service.interfaces.common;

import com.example.bookshopdemo.model.dto.UserDto;

public interface UserServiceCommon {

    UserDto findCurrentUser();

    UserDto updateUser(UserDto userDto);

    void saveUser(UserDto userDto);
}
