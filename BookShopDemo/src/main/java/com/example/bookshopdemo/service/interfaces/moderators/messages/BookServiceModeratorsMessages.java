package com.example.bookshopdemo.service.interfaces.moderators.messages;

import com.example.bookshopdemo.model.dto.BookDto;
import com.example.bookshopdemo.service.interfaces.common.messages.BookServiceCommonMessages;

public interface BookServiceModeratorsMessages extends BookServiceCommonMessages {

    String getMessageOfSaveBook(BookDto bookDto);

    String getMessageOfUpdateBook(BookDto bookDto);

    String getMessageOfDeleteBook(Long id);
}
