package com.example.bookshopdemo.service.interfaces;

import com.example.bookshopdemo.model.dto.UserDto;

import java.util.List;

public interface UserService {

    UserDto findCurrentUser();

    UserDto findUserById(Long id);

    UserDto findUserByEmail(String email);

    List<UserDto> findAllUsers();

    void saveUser(UserDto userDto);

    UserDto updateUser(UserDto userDto);

    UserDto deleteUserById(Long id);

    List<UserDto> findUserByLastName(String lastName);

    UserDto addUserRole(Long UserId, Long RoleId);

    UserDto deleteUserRole(Long UserId, Long RoleId);

    String getMessageOfCurrentUser();

    String getMessageOfUpdateUser();

    String getMessageOfUserById(Long id);

    String getMessageOfUserByEmail(String email);

    String getMessageOfUserByLastName(String lastName);

    String getMessageOfAllUsers();

    String getMessageOfAddUserRole(Long userId, Long roleId);

    String getMessageOfDeleteUserRole(Long userId, Long roleId);

    String getMessageOfDeleteUserById(Long id);
}
