package com.example.bookshopdemo.service.implementations.common.messages;

import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.repository.BookRepository;
import com.example.bookshopdemo.service.interfaces.common.messages.BookServiceCommonMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceCommonMessagesImpl implements BookServiceCommonMessages {

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceCommonMessagesImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    @Transactional
    @Override
    public String getMessageOfAllBooks() {

        List<BookEntity> books = bookRepository.findAll();

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();

        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBookById(Long id) {

        return "Book by id: " + id + " found";
    }

    @Transactional
    @Override
    public String getMessageOfBookByAuthor(String author) {

        List<BookEntity> books = bookRepository.findAllBookByAuthor(author);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBookByBookName(String bookName) {

        List<BookEntity> books = bookRepository.findAllBookByBookName(bookName);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBookByKeyword(String keyword) {

        keyword = "%" + keyword + "%";
        List<BookEntity> books = bookRepository.findBooksByKeyword(keyword);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBooksByPriceLowerMax(Integer maxPrice) {

        List<BookEntity> books = bookRepository.findBooksByPriceLowerMax(maxPrice);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBooksByPriceAboveMin(Integer minPrice) {

        List<BookEntity> books = bookRepository.findBooksByPriceAboveMin(minPrice);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfAddBookToCart(Long id) {

        return "Book by id: " + id + " added to your cart";
    }
}
