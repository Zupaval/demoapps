package com.example.bookshopdemo.service.implementations.moderators.messages;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.entity.CartEntity;

import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.service.implementations.common.messages.CartServiceCommonMessagesImpl;
import com.example.bookshopdemo.service.interfaces.moderators.messages.CartServicesModeratorsMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CartServiceModeratorsMessagesImpl extends CartServiceCommonMessagesImpl
        implements CartServicesModeratorsMessages {

    private final CartRepository cartRepository;

    @Autowired
    public CartServiceModeratorsMessagesImpl(CartRepository cartRepository, CurrentUserId currentUserId) {
        super(cartRepository, currentUserId);
        this.cartRepository = cartRepository;
    }

    @Transactional
    @Override
    public String messageOfCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId).orElseThrow();
        long quantity = cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();

        if (quantity == 0) return "Cart by id: " + cartId + " without books";
        if (quantity == 1) return "Cart by id: " + cartId + " with 1 book";
        return "Cart by id: " + cartId + " with " + quantity + " books";
    }

    @Transactional
    @Override
    public String messageOfAllCarts() {

        List<CartEntity> carts = cartRepository.findAll();
        long quantity = carts
                .stream()
                .map(CartEntity::getBooks)
                .map(books -> books.stream()
                        .map(BookEntity::getId))
                .count();

        if (quantity == 0) return "All carts without books";
        if (quantity == 1) return "All carts with 1 book";
        return "All carts with " + quantity + " books";
    }

    @Transactional
    @Override
    public String messageOfAddBookToCartById(Long cartId, Long id) {

        return "Book by id: " + id + "successfully added to cart by id: " + cartId;
    }

    @Transactional
    @Override
    public String messageOfDeleteBookFromCartById(Long cartId, Long id) {

        return "Book by id: " + id + "successfully deleted from cart by id: " + cartId;
    }

    @Transactional
    @Override
    public String messageOfDeleteAllBooksFromCartById(Long cartId) {

        CartEntity cartEntity = cartRepository.findById(cartId).orElseThrow();
        long quantity = cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();

        if (quantity == 0) return "No books in cart by id: " + cartId;
        if (quantity == 1) return "1 book successfully deleted from cart by id: " + cartId;
        return quantity + " books successfully deleted from cart by id: " + cartId;
    }
}
