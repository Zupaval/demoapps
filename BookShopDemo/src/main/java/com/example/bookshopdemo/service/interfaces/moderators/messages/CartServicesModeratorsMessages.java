package com.example.bookshopdemo.service.interfaces.moderators.messages;

import com.example.bookshopdemo.service.interfaces.common.messages.CartServiceCommonMessages;

public interface CartServicesModeratorsMessages extends CartServiceCommonMessages {

    String messageOfCartById(Long cartId);

    String messageOfAllCarts();

    String messageOfAddBookToCartById(Long cartId, Long id);

    String messageOfDeleteBookFromCartById(Long cartId, Long id);

    String messageOfDeleteAllBooksFromCartById(Long cartId);
}
