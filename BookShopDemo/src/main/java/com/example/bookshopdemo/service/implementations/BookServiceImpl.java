package com.example.bookshopdemo.service.implementations;

import com.example.bookshopdemo.config.security.CurrentUserId;
import com.example.bookshopdemo.error.BookAlreadyExistsException;
import com.example.bookshopdemo.error.NoSuchBookException;
import com.example.bookshopdemo.model.dto.BookDto;
import com.example.bookshopdemo.model.dto.mapper.BookMapper;
import com.example.bookshopdemo.model.entity.BookEntity;
import com.example.bookshopdemo.model.entity.CartEntity;
import com.example.bookshopdemo.model.repository.BookRepository;
import com.example.bookshopdemo.model.repository.CartRepository;
import com.example.bookshopdemo.service.interfaces.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    private final CartRepository cartRepository;

    private final CurrentUserId currentUserId;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, CartRepository cartRepository, CurrentUserId currentUserId) {
        this.bookRepository = bookRepository;
        this.cartRepository = cartRepository;
        this.currentUserId = currentUserId;
    }

    @Transactional
    @Override
    public BookDto findBookById(Long id) {

        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));
        return BookMapper.fromEntityToDto(bookEntity);
    }

    @Transactional
    @Override
    public List<BookDto> findBookByAuthor(String author) {

        return bookRepository.findAllBookByAuthor(author)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBookByBookName(String bookName) {

        return bookRepository.findAllBookByBookName(bookName)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findAllBooks() {

        return bookRepository.findAll()
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public BookDto saveBook(BookDto bookDto) {

        BookEntity bookEntityTemp = BookMapper.fromDtoToEntity(bookDto);
        BookEntity bookByAuthorAndBookName = bookRepository
                .findBookByAuthorAndBookName(bookEntityTemp.getAuthor(), bookEntityTemp.getBookName());

        if (bookByAuthorAndBookName != null) {
            throw new BookAlreadyExistsException("Book already exists by id: " + bookByAuthorAndBookName.getId());
        }

        BookEntity bookEntity = bookRepository.save(BookMapper.fromDtoToEntity(bookDto));
        bookDto.setId(bookEntity.getId());
        return bookDto;
    }

    @Transactional
    @Override
    public BookDto updateBook(BookDto bookDto) {

        bookRepository.findById(bookDto.getId())
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + bookDto.getId() + " not found!"));

        BookEntity bookEntityTemp = BookMapper.fromDtoToEntity(bookDto);
        BookEntity bookByAuthorAndBookName = bookRepository
                .findBookByAuthorAndBookName(bookEntityTemp.getAuthor(), bookEntityTemp.getBookName());

        if (bookByAuthorAndBookName != null) {
            if (!Objects.equals(bookByAuthorAndBookName.getId(), bookDto.getId())) {
                throw new BookAlreadyExistsException("Book with same author and name already exists by id: "
                        + bookByAuthorAndBookName.getId());
            }
        }

        BookEntity bookEntity = bookRepository.saveAndFlush(BookMapper.fromDtoToEntity(bookDto));
        return BookMapper.fromEntityToDto(bookEntity);
    }

    @Transactional
    @Override
    public BookDto deleteBookById(Long id) {

        bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));

        BookEntity bookEntity = bookRepository.deleteBookById(id);
        return BookMapper.fromEntityToDto(bookEntity);
    }

    @Transactional
    @Override
    public List<BookDto> findBooksByPriceLowerMax(Integer maxPrice) {

        return bookRepository.findBooksByPriceLowerMax(maxPrice)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBooksByPriceAboveMin(Integer minPrice) {

        return bookRepository.findBooksByPriceAboveMin(minPrice)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> addBookToCart(Long id) {

        BookEntity bookEntity = bookRepository.findById(id)
                .orElseThrow(() -> new NoSuchBookException("Book by id: " + id + " not found!"));

        CartEntity cartEntityTemp = cartRepository.findById(currentUserId.getUserId()).orElseThrow();

        List<BookEntity> books = cartEntityTemp.getBooks();
        books.add(bookEntity);
        cartEntityTemp.setBooks(books);
        cartRepository.saveAndFlush(cartEntityTemp);

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseThrow();

        return cartEntity.getBooks()
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBookByKeyword(String keyword) {

        keyword = "%" + keyword + "%";
        return bookRepository.findBooksByKeyword(keyword)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public String getMessageOfAllBooks() {

        List<BookEntity> books = bookRepository.findAll();

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();

        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBookById(Long id) {

        return "Book by id: " + id + " found";
    }

    @Transactional
    @Override
    public String getMessageOfBookByAuthor(String author) {

        List<BookEntity> books = bookRepository.findAllBookByAuthor(author);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBookByBookName(String bookName) {

        List<BookEntity> books = bookRepository.findAllBookByBookName(bookName);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBookByKeyword(String keyword) {

        keyword = "%" + keyword + "%";
        List<BookEntity> books = bookRepository.findBooksByKeyword(keyword);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBooksByPriceLowerMax(Integer maxPrice) {

        List<BookEntity> books = bookRepository.findBooksByPriceLowerMax(maxPrice);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfBooksByPriceAboveMin(Integer minPrice) {

        List<BookEntity> books = bookRepository.findBooksByPriceAboveMin(minPrice);

        long quantity = books.stream()
                .map(BookEntity::getId)
                .count();
        if (quantity == 0) return "No books found";
        if (quantity == 1) return "1 book found";
        return quantity + " books found";
    }

    @Transactional
    @Override
    public String getMessageOfAddBookToCart(Long id) {

        return "Book by id: " + id + " added to your cart";
    }

    @Transactional
    @Override
    public String getMessageOfSaveBook(BookDto bookDto) {

        BookEntity bookEntity = BookMapper.fromDtoToEntity(bookDto);
        BookEntity bookByAuthorAndBookName = bookRepository
                .findBookByAuthorAndBookName(bookEntity.getAuthor(), bookEntity.getBookName());

        return "Book by id: " + bookByAuthorAndBookName.getId() + " successfully saved";
    }

    @Transactional
    @Override
    public String getMessageOfUpdateBook(BookDto bookDto) {

        return "Book by id: " + bookDto.getId() + " successfully updated";
    }

    @Transactional
    @Override
    public String getMessageOfDeleteBook(Long id) {
        return "Book by id: " + id + " successfully deleted";
    }
}
