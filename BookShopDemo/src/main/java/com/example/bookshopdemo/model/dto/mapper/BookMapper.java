package com.example.bookshopdemo.model.dto.mapper;

import com.example.bookshopdemo.model.dto.BookDto;
import com.example.bookshopdemo.model.entity.BookEntity;

public class BookMapper {

    public static BookDto fromEntityToDto(BookEntity bookEntity) {

        BookDto bookDto = new BookDto();
        bookDto.setId(bookEntity.getId());
        bookDto.setAuthor(bookEntity.getAuthor());
        bookDto.setBookName(bookEntity.getBookName());
        bookDto.setPrice(bookEntity.getPrice());
        return bookDto;
    }

    public static BookEntity fromDtoToEntity(BookDto bookDto) {

        BookEntity bookEntity = new BookEntity();
        bookEntity.setId(bookDto.getId());
        bookEntity.setAuthor(bookDto.getAuthor());
        bookEntity.setBookName(bookDto.getBookName());
        bookEntity.setPrice(bookDto.getPrice());
        return bookEntity;
    }
}
