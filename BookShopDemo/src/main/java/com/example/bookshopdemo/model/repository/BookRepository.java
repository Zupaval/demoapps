package com.example.bookshopdemo.model.repository;

import com.example.bookshopdemo.model.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {

    List<BookEntity> findAllBookByAuthor(String author);

    List<BookEntity> findAllBookByBookName(String bookName);

    @Query(value = "select * from books where author like :keyword or book_name like :keyword", nativeQuery = true)
    List<BookEntity> findBooksByKeyword(String keyword);

    @Query(value = "select * from books where price <= :maxPrice order by price desc", nativeQuery = true)
    List<BookEntity> findBooksByPriceLowerMax(Integer maxPrice);

    @Query(value = "select * from books where price >= :minPrice order by price", nativeQuery = true)
    List<BookEntity> findBooksByPriceAboveMin(Integer minPrice);

    @Query(value = "delete from books where id = :id returning *", nativeQuery = true)
    BookEntity deleteBookById(Long id);

    @Query(value = "select * from books where author = :author and book_name = :bookName", nativeQuery = true)
    BookEntity findBookByAuthorAndBookName(String author, String bookName);

}
