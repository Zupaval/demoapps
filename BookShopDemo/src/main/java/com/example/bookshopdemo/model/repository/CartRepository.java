package com.example.bookshopdemo.model.repository;

import com.example.bookshopdemo.model.entity.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CartRepository extends JpaRepository<CartEntity, Long> {

}
