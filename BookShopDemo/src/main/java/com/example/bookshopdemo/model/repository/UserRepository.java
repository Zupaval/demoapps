package com.example.bookshopdemo.model.repository;

import com.example.bookshopdemo.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findUserByEmail(String email);

    List<UserEntity> findAllUserByLastName(String lastName);

    @Query(value = "delete from users where id = :id returning *", nativeQuery = true)
    UserEntity deleteUserById(Long id);

    @Modifying
    @Query(value = "insert into users_roles (user_id, role_id) values (:userId, :roleId)", nativeQuery = true)
    void addUserRole(Long userId, Long roleId);

    @Modifying
    @Query(value = "delete from users_roles where user_id = :userId and role_id = :roleId", nativeQuery = true)
    void removeUserRole(Long userId, Long roleId);
}
