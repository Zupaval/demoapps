package com.example.bookshoprest.repository;

import com.example.bookshoprest.model.entity.BookEntity;
import com.example.bookshoprest.model.repository.BookRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BookRepositoryTest {

    @Autowired
    BookRepository bookRepository;

    @Test
    @DisplayName("test for find books by author")
    public void findAllBookByAuthorTest() {

        List<BookEntity> books = bookRepository.findAllBookByAuthor("Агата Кристи");
        assertEquals(3, books.size());
    }

    @Test
    @DisplayName("test for find books by book's name")
    public void findAllBookByBookNameTest() {

        List<BookEntity> books = bookRepository.findAllBookByBookName("Немезида");
        assertEquals(2, books.size());
    }

    @Test
    @DisplayName("test for find books by a price less than max")
    public void findBooksByPriceLowerMaxTest() {

        List<BookEntity> booksLowerMax = bookRepository.findBooksByPriceLowerMax(200);
        assertEquals(3, booksLowerMax.size());

    }
}