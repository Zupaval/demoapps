package com.example.bookshoprest.error;

public class NotSpecifiedIdException extends RuntimeException{

    public NotSpecifiedIdException(String message) {
        super(message);
    }
}
