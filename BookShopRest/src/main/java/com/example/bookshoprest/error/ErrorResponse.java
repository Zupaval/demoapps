package com.example.bookshoprest.error;

import java.util.List;

public class ErrorResponse {

    final private List<Violation> violations;

    public ErrorResponse(List<Violation> violations) {
        this.violations = violations;
    }

    public List<Violation> getViolations() {
        return violations;
    }
}