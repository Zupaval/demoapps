package com.example.bookshoprest.controller;

import com.example.bookshoprest.model.dto.CartDto;
import com.example.bookshoprest.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {

    private CartService cartService;

    @Autowired
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/id/{id}")
    public CartDto findCartById(@PathVariable Long id) {
        return cartService.findCartById(id);
    }

    @GetMapping("/cart")
    public CartDto findCart() {
        return cartService.findCart();
    }

    @GetMapping("/all")
    List<CartDto> findAllCarts() {
        return cartService.findAllCarts();
    }

    @GetMapping("/total/coast")
    public Integer totalCoast() {
        return cartService.totalCoast();
    }

    @GetMapping("/total/quantity")
    public Long totalQuantity() {
        return cartService.totalQuantity();
    }

    @PostMapping("/create")
    public CartDto createCar(@RequestBody CartDto cartDto) {
        return cartService.saveCart(cartDto);
    }

    @PutMapping("/update")
    public CartDto updateCar(@RequestBody CartDto cartDto) {
        return cartService.updateCart(cartDto);
    }

    @DeleteMapping("/delete/id/{id}")
    public boolean deleteCartById(@PathVariable Long id) {
        return cartService.deleteCartById(id);
    }

    @DeleteMapping("/delete}")
    public boolean deleteCart() {
        return cartService.deleteCart();
    }
}