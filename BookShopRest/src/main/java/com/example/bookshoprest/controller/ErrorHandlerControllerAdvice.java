package com.example.bookshoprest.controller;

import com.example.bookshoprest.error.ErrorResponse;
import com.example.bookshoprest.error.Violation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ErrorHandlerControllerAdvice {

    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse onConstraintError(ConstraintViolationException exception) {

        List<Violation> violations = exception.getConstraintViolations()
                .stream()
                .map(v -> new Violation(v.getPropertyPath().toString(), v.getMessage()))
                .collect(Collectors.toList());
        return new ErrorResponse(violations);
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse onMethodArgumentNotValidError(MethodArgumentNotValidException exception) {

        List<Violation> violations = exception.getBindingResult().getFieldErrors()
                .stream()
                .map(v -> new Violation(v.getField(), v.getDefaultMessage()))
                .collect(Collectors.toList());
        return new ErrorResponse(violations);
    }
}
