package com.example.bookshoprest.controller;

import com.example.bookshoprest.model.dto.BookDto;
import com.example.bookshoprest.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    private BookService bookService;

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/id/{id}")
    public BookDto findBookById(@PathVariable Long id) {
        return bookService.findBookById(id);
    }

    @GetMapping("author/{author}")
    public List<BookDto> findBookByAuthor(@PathVariable String author) {
        return bookService.findBookByAuthor(author);
    }

    @GetMapping("/bookName/{bookName}")
    public List<BookDto> findBookByBookName(@PathVariable String bookName) {
        return bookService.findBookByBookName(bookName);
    }

    @GetMapping("/all")
    public List<BookDto> findAllBooks() {
        return bookService.findAllBooks();
    }

    @PostMapping("/create")
    public BookDto createBook(@RequestBody BookDto bookDto) {
        return bookService.saveBook(bookDto);
    }

    @PutMapping("/update")
    public BookDto updateUser(@RequestBody BookDto bookDto) {
        return bookService.updateBook(bookDto);
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteUserById(@PathVariable Long id) {
        return bookService.deleteBookById(id);
    }

    @GetMapping("/price/less/{maxPrice}")
    public List<BookDto> findBookByPriceLowerMaxPrice(@PathVariable Integer maxPrice) {
        return bookService.findBooksByPriceLowerMax(maxPrice);
    }

    @GetMapping("/price/more/{minPrice}")
    public List<BookDto> findBookByPriceAboveMinPrice(@PathVariable Integer minPrice) {
        return bookService.findBooksByPriceAboveMin(minPrice);
    }
}
