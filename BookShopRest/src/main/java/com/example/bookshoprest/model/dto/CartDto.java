package com.example.bookshoprest.model.dto;

import com.example.bookshoprest.model.entity.BookEntity;

import java.util.List;
import java.util.Objects;

public class CartDto {

    private Long id;

    private List<BookEntity> books;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<BookEntity> getBooks() {
        return books;
    }

    public void setBooks(List<BookEntity> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartDto cartDto = (CartDto) o;
        return Objects.equals(id, cartDto.id) && Objects.equals(books, cartDto.books);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, books);
    }

    @Override
    public String toString() {
        return "CartDto{" +
                "id=" + id +
                ", books=" + books +
                '}';
    }
}
