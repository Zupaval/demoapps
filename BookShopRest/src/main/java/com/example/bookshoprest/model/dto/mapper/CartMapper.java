package com.example.bookshoprest.model.dto.mapper;

import com.example.bookshoprest.model.dto.CartDto;
import com.example.bookshoprest.model.entity.CartEntity;
import org.springframework.stereotype.Component;

@Component
public class CartMapper {

    public static CartDto fromEntityToDto(CartEntity cartEntity) {

        CartDto cartDto = new CartDto();
        cartDto.setId(cartEntity.getId());
        cartDto.setBooks(cartEntity.getBooks());
        return cartDto;
    }

    public static CartEntity fromDtoToEntity(CartDto cartDto) {

        CartEntity cartEntity = new CartEntity();
        cartEntity.setId(cartDto.getId());
        cartEntity.setBooks(cartDto.getBooks());
        return cartEntity;
    }
}
