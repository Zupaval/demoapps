package com.example.bookshoprest.model.repository;

import com.example.bookshoprest.model.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {

    List<BookEntity> findAllBookByAuthor(String author);

    List<BookEntity> findAllBookByBookName(String bookName);

    @Query(value = "select * from books b where b.price <= :maxPrice order by b.price desc", nativeQuery = true)
    List<BookEntity> findBooksByPriceLowerMax(Integer maxPrice);

    @Query(value = "select * from books b where b.price >= :minPrice order by b.price", nativeQuery = true)
    List<BookEntity> findBooksByPriceAboveMin(Integer minPrice);

}
