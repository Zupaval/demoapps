package com.example.bookshoprest.model.dto.mapper;

import com.example.bookshoprest.model.dto.UserDto;
import com.example.bookshoprest.model.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public static UserDto fromEntityToDto(UserEntity userEntity) {

        UserDto userDto = new UserDto();
        userDto.setId(userEntity.getId());
        userDto.setFirstName(userEntity.getFirstName());
        userDto.setPatronymicName(userEntity.getPatronymicName());
        userDto.setLastName(userEntity.getLastName());
        userDto.setEmail(userEntity.getEmail());
        userDto.setPassword(userEntity.getPassword());
        userDto.setRoles(userEntity.getRoles());
        return userDto;
    }

    public static UserEntity fromDtoToEntity(UserDto userDto) {

        UserEntity userEntity = new UserEntity();
        userEntity.setId(userDto.getId());
        userEntity.setFirstName(userDto.getFirstName());
        userEntity.setPatronymicName(userDto.getPatronymicName());
        userEntity.setLastName(userDto.getLastName());
        userEntity.setEmail(userDto.getEmail());
        userEntity.setPassword(userDto.getPassword());
        userEntity.setRoles(userDto.getRoles());
        return userEntity;
    }
}
