package com.example.bookshoprest.service;

import com.example.bookshoprest.model.dto.CartDto;

import java.util.List;

public interface CartService {

    CartDto findCartById(Long id);

    CartDto findCart();

    List<CartDto> findAllCarts();

    CartDto saveCart(CartDto cartDto);

    CartDto updateCart(CartDto cartDto);

    boolean deleteCartById(Long id);

    boolean deleteCart();

    Integer totalCoast();

    Long totalQuantity();
}
