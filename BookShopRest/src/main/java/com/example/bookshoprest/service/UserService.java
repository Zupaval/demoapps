package com.example.bookshoprest.service;

import com.example.bookshoprest.model.dto.UserDto;

import java.util.List;

public interface UserService {

    UserDto findUserById(Long id);

    UserDto findUserByEmail(String email);

    List<UserDto> findAllUsers();

    UserDto saveUser(UserDto userDto);

    UserDto updateUser(UserDto userDto);

    boolean deleteUserById(Long id);

    UserDto getMyAccount();
}
