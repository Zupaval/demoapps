package com.example.bookshoprest.service;

import com.example.bookshoprest.config.security.CurrentUserId;
import com.example.bookshoprest.model.dto.CartDto;
import com.example.bookshoprest.model.dto.mapper.CartMapper;
import com.example.bookshoprest.model.entity.BookEntity;
import com.example.bookshoprest.model.entity.CartEntity;
import com.example.bookshoprest.model.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    private CartRepository cartRepository;

    private CurrentUserId currentUserId;

    @Autowired
    public void setCurrentUserId(CurrentUserId currentUserId) {
        this.currentUserId = currentUserId;
    }

    @Autowired
    public void setCartRepository(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Transactional
    @Override
    public CartDto findCartById(Long id) {

        CartEntity cartEntity = cartRepository.findById(id).orElseGet(CartEntity::new);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Override
    public CartDto findCart() {
        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseGet(CartEntity::new);
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public List<CartDto> findAllCarts() {

        return cartRepository.findAll()
                .stream()
                .map(CartMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public CartDto saveCart(CartDto cartDto) {

        cartDto.setId(currentUserId.getUserId());
        CartEntity cartEntity = cartRepository.save(CartMapper.fromDtoToEntity(cartDto));
        cartDto.setId(cartEntity.getId());
        return cartDto;
    }

    @Transactional
    @Override
    public CartDto updateCart(CartDto cartDto) {

        cartDto.setId(currentUserId.getUserId());
        CartEntity cartEntity = cartRepository.saveAndFlush(CartMapper.fromDtoToEntity(cartDto));
        return CartMapper.fromEntityToDto(cartEntity);
    }

    @Transactional
    @Override
    public boolean deleteCartById(Long id) {

        cartRepository.deleteById(id);
        return !cartRepository.existsById(id);
    }

    @Override
    public boolean deleteCart() {

        cartRepository.deleteById(currentUserId.getUserId());
        return !cartRepository.existsById(currentUserId.getUserId());
    }

    @Override
    public Integer totalCoast() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseGet(CartEntity::new);
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getPrice)
                .reduce(Integer::sum).orElse(0);
    }

    @Override
    public Long totalQuantity() {

        CartEntity cartEntity = cartRepository.findById(currentUserId.getUserId()).orElseGet(CartEntity::new);
        return cartEntity.getBooks()
                .stream()
                .map(BookEntity::getId)
                .count();
    }
}
