package com.example.bookshoprest.service;

import com.example.bookshoprest.error.NotSpecifiedIdException;
import com.example.bookshoprest.model.dto.BookDto;
import com.example.bookshoprest.model.dto.mapper.BookMapper;
import com.example.bookshoprest.model.entity.BookEntity;
import com.example.bookshoprest.model.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Transactional
    @Override
    public BookDto findBookById(Long id) {

        BookEntity bookEntity = bookRepository.findById(id).orElseGet(BookEntity::new);
        return BookMapper.fromEntityToDto(bookEntity);
    }

    @Transactional
    @Override
    public List<BookDto> findBookByAuthor(String author) {

        return bookRepository.findAllBookByAuthor(author)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBookByBookName(String bookName) {

        return bookRepository.findAllBookByBookName(bookName)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findAllBooks() {

        return bookRepository.findAll()
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public BookDto saveBook(@Valid BookDto bookDto) {

        BookEntity bookEntity = bookRepository.save(BookMapper.fromDtoToEntity(bookDto));
        bookDto.setId(bookEntity.getId());
        return bookDto;
    }

    @Transactional
    @Override
    public BookDto updateBook(@Valid BookDto bookDto) {

        if (bookDto.getId() == null) {
            throw new NotSpecifiedIdException("Не указан id");
        }

        BookEntity bookEntity = bookRepository.saveAndFlush(BookMapper.fromDtoToEntity(bookDto));
        return BookMapper.fromEntityToDto(bookEntity);
    }

    @Transactional
    @Override
    public boolean deleteBookById(Long id) {

        bookRepository.deleteById(id);
        return !bookRepository.existsById(id);
    }

    @Transactional
    @Override
    public List<BookDto> findBooksByPriceLowerMax(Integer maxPrice) {

        return bookRepository.findBooksByPriceLowerMax(maxPrice)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<BookDto> findBooksByPriceAboveMin(Integer minPrice) {

        return bookRepository.findBooksByPriceAboveMin(minPrice)
                .stream()
                .map(BookMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }
}
