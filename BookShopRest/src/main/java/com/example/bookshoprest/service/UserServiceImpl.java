package com.example.bookshoprest.service;

import com.example.bookshoprest.config.security.CurrentUserId;
import com.example.bookshoprest.error.NotSpecifiedIdException;
import com.example.bookshoprest.model.dto.UserDto;
import com.example.bookshoprest.model.dto.mapper.UserMapper;
import com.example.bookshoprest.model.entity.UserEntity;
import com.example.bookshoprest.model.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private CurrentUserId currentUserId;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setCurrentUserId(CurrentUserId currentUserId) {
        this.currentUserId = currentUserId;
    }

    @Transactional
    @Override
    public UserDto findUserById(Long id) {

        UserEntity userEntity = userRepository.findById(id).orElseGet(UserEntity::new);
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto findUserByEmail(String email) {

        UserEntity userEntity = userRepository.findUserByEmail(email).orElseGet(UserEntity::new);
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public List<UserDto> findAllUsers() {

        return userRepository.findAll()
                .stream()
                .map(UserMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public UserDto saveUser(@Valid UserDto userDto) {

        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        UserEntity userEntity = userRepository.save(UserMapper.fromDtoToEntity(userDto));
        userDto.setId(userEntity.getId());
        return userDto;
    }

    @Transactional
    @Override
    public UserDto updateUser(@Valid UserDto userDto) {

        if (userDto.getId() == null) {

            throw new NotSpecifiedIdException("Не указан id");
        }

        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        UserEntity userEntity = userRepository.saveAndFlush(UserMapper.fromDtoToEntity(userDto));
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public boolean deleteUserById(Long id) {

        userRepository.deleteById(id);
        return !userRepository.existsById(id);
    }

    @Override
    public UserDto getMyAccount() {

        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseGet(UserEntity::new);
        return UserMapper.fromEntityToDto(userEntity);
    }
}
