package com.example.bookshoprest.service;

import com.example.bookshoprest.model.dto.BookDto;

import java.util.List;

public interface BookService {

    BookDto findBookById(Long id);

    List<BookDto> findBookByAuthor(String author);

    List<BookDto> findBookByBookName(String bookName);

    List<BookDto> findAllBooks();

    BookDto saveBook(BookDto bookDto);

    BookDto updateBook(BookDto bookDto);

    boolean deleteBookById(Long id);

    List<BookDto> findBooksByPriceLowerMax(Integer maxPrice);

    List<BookDto> findBooksByPriceAboveMin(Integer minPrice);


}
