package com.example.bookshoprest.config.security;

import com.example.bookshoprest.model.entity.UserEntity;
import com.example.bookshoprest.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CurrentUserId {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Long getUserId() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        UserEntity userEntity = userRepository.findUserByEmail(name).orElseThrow();
        return userEntity.getId();
    }
}
