package com.example.bookshoprest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@PropertySource(value = {"classpath:bookShopRest.properties"},
        ignoreResourceNotFound = true)
public class AppConfig {

}
