package com.example.bookshoprest.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    public void authConfigure(
            AuthenticationManagerBuilder auth,
            PasswordEncoder encoder,
            UserAuthService userAuthService
    ) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(encoder.encode("admin"))
                .roles("ADMIN");

        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userAuthService);
        provider.setPasswordEncoder(encoder);
        auth.authenticationProvider(provider);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/book/all", "/book/id/**", "/book/author/**", "/book/bookName/**", "/book/price/**")
                .hasAnyRole("USER", "ADMIN")
                .antMatchers("/book/**")
                .hasAnyRole("ADMIN")
                .antMatchers("/user/account")
                .hasAnyRole("USER", "ADMIN")
                .antMatchers("/user/**")
                .hasAnyRole("ADMIN")
                .antMatchers("/cart/id/**", "/cart/all", "/cart/delete/**")
                .hasAnyRole("ADMIN")
                .antMatchers("/cart/cart", "cart/create", "/cart/update", "/cart/delete", "/cart/total/**")
                .hasAnyRole("ADMIN", "USER")
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        return http.build();
    }
}
