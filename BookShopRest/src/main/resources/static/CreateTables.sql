create table users
(
    id              serial primary key,
    first_name      varchar(255) not null,
    patronymic_name varchar(255),
    last_name       varchar(255) not null,
    email           varchar(255) not null unique,
    password        varchar(255) not null
);

create table roles
(
    id   serial primary key,
    name varchar(255) not null
);

create table users_roles
(
    id      serial primary key,
    user_id bigint,
    role_id bigint
);

create table books
(
    id        serial primary key,
    author    varchar(255) not null,
    book_name varchar(255) not null,
    price     integer      not null
);

create table carts
(
    id bigint primary key,
    CONSTRAINT carts_users_fk
        FOREIGN KEY (id) REFERENCES users (id) on delete cascade

);

create table carts_books
(
    cart_id bigint,
    book_id bigint
);